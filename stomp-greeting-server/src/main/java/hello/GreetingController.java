package hello;

import hello.pojo.Greeting;
import hello.pojo.HelloMessage;
import hello.pojo.MyBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class GreetingController {

    private final SimpMessagingTemplate template;
    private final MyBean myBean;

    @Autowired
    public GreetingController(SimpMessagingTemplate template, MyBean myBean) {
        this.template = template;
        this.myBean = myBean;
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message, SimpMessageHeaderAccessor headerAccessor, Principal user) throws Exception {
        System.out.println(headerAccessor.getSessionAttributes());
        System.out.println("Sending Greeting");
        System.out.println(user);
        Thread.sleep(3000); // simulated delay
        return new Greeting("Hello, " + message.getName() + "!");
    }


    //    @RequestMapping(path="/greeti", method=GET)
    @Scheduled(fixedRate = 5000)
    public void greet() {
        String text = "neco";
        this.template.convertAndSend("/topic/greetings", text);
    }

    @Scheduled(fixedRate = 5000)
    public void sendToUser() {
        String text = "neco";
        this.template.convertAndSend("/topic/greetings", text);
//        this.template.convertAndSendToUser();
    }
}