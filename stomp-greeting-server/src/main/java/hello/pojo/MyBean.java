package hello.pojo;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Scope(scopeName = "websocket", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MyBean {

    //To store user properties or not

    @PostConstruct
    public void init() {
        // Invoked after dependencies injected
    }

    @PreDestroy
    public void destroy() {
        // Invoked when the WebSocket session ends
    }
}